package net.dasturlash.dilshod1;

/**
 * Created by Sherzodbek on 19.03.2017.
 */

public class MyData {
    private String header;
    private String subText;

    public MyData(String header, String subText) {
        this.header = header;
        this.subText = subText;
    }

    public String getHeader() {
        return header;
    }

    public String getSubText() {
        return subText;
    }
}
